#!/bin/bash
if [[ "$1" == '-d' ]]; then
    LEVEL="DEBUG"
else
    LEVEL="PROD"
fi
ESELECT_NEW_VERSION=`eselect kernel list | tail -1 | sed -r -n 's/.*\[([1-9][0-9]+)\].*/\1/p'`
ESELECT_CURRENT_VERSION=`eselect kernel list | egrep '\*' | sed -rn 's/.*\[([1-9][0-9]+)\].*/\1/p'`

RUNNING_KERNEL_VERSION=`uname -a | sed -rn 's/.*([1-9]{1,3}(\.[1-9]{1,3}){2,3}-gentoo)-x86_64.*/\1/p'`
CURRENT_KERNEL_VERSION=`eselect kernel list | egrep '\*'| sed -rn 's/.*linux-([[:alpha:][:digit:].-]+)(\s\*)?/\1/p'`
NEW_KERNEL_VERSION=`eselect kernel list | tail -1 | sed -rn 's/.* linux-([[:alpha:][:digit:].-]+)(\s\*)?/\1/p'`
LASTEST_KERNEL_BUILT=`ls /boot -t | grep vmlinuz | head -1 | sed -rn 's/.*([1-9]{1,3}(\.[1-9]{1,3}){2,3}-gentoo)-x86_64/\1/p'`
BOOTLOADER_KERNEL_VERSION=`grep vmlinuz /boot/grub/grub.cfg | head -1 | sed -rn 's/.*([1-9]{1,3}(\.[1-9]{1,3}){2,3}-gentoo).*/\1/p'`
cd /usr/src 
GIT_KERNEL_VERSION=`git log --oneline -1 | sed -rn 's/.* ([[:alnum:].-]+-gentoo)/\1/p'`
GIT_UNFINISHED=`git status | tail -1`


if [[ "$LATEST_KERNEL_BUILT" == "$NEW_KERNEL_VERSION" ]]; then
    IS_LATEST_BUILT='TRUE'
else 
    IS_LATEST_BUILT='FALSE'
fi

if [[ "${RUNNING_KERNEL_VERSION}" == "${NEW_KERNEL_VERSION}" ]]; then
    IS_LATEST_RUNNING='TRUE'
else 
    IS_LATEST_RUNNING='FALSE'
fi

if [[ "${GIT_KERNEL_VERSION}" == "${NEW_KERNEL_VERSION}" ]]; then
    IS_LATEST_COMMITTED='TRUE'
else 
    IS_LATEST_COMMITTED='FALSE'
fi

if [[ "${GIT_KERNEL_VERSION}" == "${RUNNING_KERNEL_VERSION}" ]]; then
    IS_RUNNING_COMMITTED='TRUE'
else 
    IS_RUNNING_COMMITTED='FALSE'
fi

if [[ "${BOOTLOADER_KERNEL_VERSION}" == "${LATEST_KERNEL_VERSION}" ]]; then
    IS_LATEST_IN_GRUB='TRUE'
else
    IS_LATEST_IN_GRUB='FALSE'
fi

if [[ "${GIT_UNFINISHED}" == "nothing to commit, working tree clean" ]]; then
    IS_GIT_UNFINISHED='FALSE'
else
    IS_GIT_UNFINISHED='TRUE'
fi

commit_kernel_version() {
    local KERNEL_VERSION=$1
    ###
    echo "Moving to main folder 'usr/src'"
    cd /usr/src/
    
    
    ###
    echo "copy the previous kernel .config"
    cd cp ./linux-${KERNEL_VERSION}/.config config

    ###
    echo "commit it to git"

    git add config
    git commit -m "Kernel version: ${KERNEL_VERSION}"
}

compile_latest_kernel_version() {
    eselect kernel set ${ESELECT_NEW_VERSION}
    cd /usr/src
    echo "Starting from version ${GIT_KERNEL_VERSION}" 
    cp config ./linux/.config
    cd linux
    echo  "Compiling"
    head -5 Makefile
    echo "... starts in"
    sleep 1
    echo " ."
    sleep 1
    echo "\r .."
    echo "\r ..."
    genkernel --menuconfig all
}

if [[ "$LEVEL" == "DEBUG" ]]; then
    echo "--- KERKNELS"
    echo "RUNNING_KERNEL_VERSION='${RUNNING_KERNEL_VERSION}'"
    echo "CURRENT_KERNEL_VERSION='${CURRENT_KERNEL_VERSION}'"
    echo "NEW_KERNEL_VERSION='${NEW_KERNEL_VERSION}'"
    echo "LASTEST_KERNEL_BUILT='${LASTEST_KERNEL_BUILT}'"
    echo "GIT_KERNEL_VERSION='${GIT_KERNEL_VERSION}'"
    echo "BOOTLOADER_KERNEL_VERSION='${BOOTLOADER_KERNEL_VERSION}'"
    
    echo "--- ESELECT"
    echo "ESELECT_CURRENT_VERSION='${ESELECT_CURRENT_VERSION}' (${CURRENT_KERNEL_VERSION})"
    echo "ESELECT_NEW_VERSION='${ESELECT_NEW_VERSION}' (${NEW_KERNEL_VERSION})"
    echo "GIT_UNFINISHED='${GIT_UNFINISHED}'"
    echo "--- CHECKS"
    echo "IS_LATEST_BUILT='${IS_LATEST_BUILT}'"
    echo "IS_LATEST_RUNNING='${IS_LATEST_RUNNING}'"
    echo "IS_LATEST_COMMITTED='${IS_LATEST_COMMITTED}'"
    echo "IS_RUNNING_COMMITTED='${IS_RUNNING_COMMITTED}'"
    echo "IS_GIT_UNFINISHED='${IS_GIT_UNFINISHED}'"
    echo "IS_LATEST_IN_GRUB='${IS_LATEST_IN_GRUB}'"
    exit
fi

if [[${IS_RUNNING_COMMITTED} == 'FALSE' ]]; then
    echo "Running kernel has not been committed. Committing configuration."
    commit_kernel_version "${RUNNING_KERNEL_VERSION}"
fi

if [[${IS_LATEST_BUILT} == 'FALSE' ]]; then
    echo "Buildining the kernel"
    compile_latest_kernel_version
fi
if [[${IS_LATEST_COMMITTED} == 'FALSE' ]]; then
    echo "Latest kernel has been built but the configuration is not committed. Committing configuration."
    commit_kernel_version ${LASTEST_KERNEL_BUILT}
fi
if [[${IS_LATEST_IN_GRUB} == 'FALSE' ]]; then
    echo "Update grub manually and reboot"
elif [[${IS_LATEST_RUNNING} == 'FALSE' && ${IS_LATEST_BUILT} == 'TRUE' ]]; then
    echo "Kernel is ready to run reboot"
fi
    
