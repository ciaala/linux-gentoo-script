#!/bin/sh
echo "Starting to Update/Rebuild Gentoo"
export _COUNTER=0
export _START='none'
while [ ${_COUNTER} -lt 2 ]
do
	echo "#### ROUND #${_COUNTER}" `date` "[previous ${_START}]"
	_START=`date`
	echo "#### Updating portage {eix-sync -q}"
	eix-sync -q
	echo "#### Emerge world"
	emerge -DuUNt --backtrack=1110 @world --keep-going --jobs=8 --load-average=11 --verbose-conflicts --exclude=www-client/chromium
	#--exclude=opencv
	echo "####  revdep-rebuild"
 	revdep-rebuild
 	emerge @preserved-rebuild
	echo "#### perl-cleaner"
 	perl-cleaner --modules
	echo "#### eclean distfiles"
 	eclean distfiles
	_COUNTER=$((_COUNTER + 1))
done

