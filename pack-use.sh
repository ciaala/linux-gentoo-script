#!/bin/sh

function check_name_is_package(){
	count_packages=`portageq best_visible / ${1} | wc -l`
	isMoreThanZero=$(( ${count_packages} > 0 ? 1 : 0 ))
	echo ${isMoreThanZero}
}
pattern='(\w+)/(\w+)'

if [[ $( check_name_is_package ${1} ) -eq 1 ]]; 
then
	tokens=(${1//// })
	echo "Editing package ${tokens[0]}/${tokens[1]} use file"
	folder="/etc/portage/package.use/${tokens[0]}"
	mkdir -p ${folder}
	filename=${folder}/${tokens[1]}.use 	
	echo "${1} ${*:2}" > ${filename}
	cat ${filename}
else
	echo "'${1}' is not a known package"
fi

